class Armstech < CraftingSkill
  attr_reader   :level
  attr_accessor :character

  def initialize(args={})
    @character = args.fetch(:character)
    @level     = 0
  end

  def craft!

    AssaultShrieker.craft!

  end

  private

  def improve
    self.level.increment
  end
end

class Schematics

  def self.all
    {
      assault_shrieker: {
        name:           'Assault Shrieker',
        crafting_skill: 'Armstech',
        quality:        'premium',
      }
    }
  end

end


