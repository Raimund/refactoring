# Preparers
# GamePreparer
# GameDefaultsPreparer
# DeckPreparer
# PlayerPreparer
# CurrentPlayerPreparer
# CardrowPreparer

# examples
# [x, x, x, x, x, x, x, x, x, x, x, x, x]
#
# 2 players
# [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
# [x, x, x, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
# [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, x, x, x]
# [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 0, 1, 2]


# context setup_cardrow

# prepares the cardrow for the next turn
# def setup_cardrow
#   self.cardrow_position_to_replenish = 0
#   remove_cards
#   move_cards
#   replenish_cards
#   # positions still empty fill with nil
# end

# discards the left 1-3 cards on the cardtimeline
# def remove_cards
#   (0..(4 - number_of_players)).each { |pos| civilcardingames.find(cardtimeline[pos]).position!("x") if cardtimeline[pos] != "x" }
# end

# # move cards left, start with position 2,3 or 4 (with 4,3 or 2 players)
# # if there is a card on that position
# # set its position on first empty field from left
# # move free field on cardrow right by one
# # set old position on "no card"
# def move_cards_left(first_empty)
#   for i in (5 - number_of_players)..12
#     if cardtimeline[i] != "x"
#       self.cardtimeline[first_empty] = cardtimeline[i] 
#       first_empty += 1
#       self.cardtimeline[i] = "x"
#     end
#   end
#   first_empty
# end

# # pick available cards
# def draw(deck, number, first_empty)
#   1.upto(number) do
#     civilcardingame = deck.delete_at(rand(deck.size))
#     self.cardtimeline[first_empty] = civilcardingame.id
#     civilcardingame.position!("cardtimeline")
#     first_empty += 1
#   end
#   first_empty
# end

# manage cardrow
# def pick_new_civilcards

#   # erstes freies feld im zeitstrahl
#   first_empty   = 0

#   # find all cards of game with position deck from db
#   current_deck  = civilcardingames.position("deck")
#   random_cards  = Array.new
  
#   remove_cards
#   first_empty = move_cards_left(first_empty)

#   cards_to_draw = 13 - first_empty
#   cards_to_draw_from_new_deck = 0

#   if current_deck.size < cards_to_draw #falls nicht mehr genug karten in stapel
#     if age == "II"# wenn letztes zeitalter schon erreicht, #dann spielende einleiten
#       self.round = 99
#     else
#       #restliche civikarten vom abgelaufenen zeitalter ziehen
#       first_empty = draw(current_deck, current_deck.size, first_empty)
#       drawn_from_old = current_deck.size

#       advance_age# if game.age < "III"#zeitalter hochzählen
#       add_age# if game.age < "II"#civilcards hinzufügen

#       #restliche civikarten aus neuem zeitalter ziehen
#       new_deck = civilcardingames.position("deck")
#       first_empty = draw(new_deck, cards_to_draw - drawn_from_old, first_empty)
#     end
#   else
#     first_empty = draw(current_deck, cards_to_draw, first_empty)
#   end

#   if first_empty < 13 #falls nicht genug karten aufgefüllt wurden rest mit nil füllen
#     for i in first_empty..12
#       self.cardtimeline[i] = "x"
#     end  
#   end
#   save!
#   cardtimeline
# end

#   describe '#setup_deck' do
#     xit 'sets up and returns a deck for passed game' do
#       game = Game.new age: 'A'
#       deck_setup = DeckSetup.new
#       expect(deck_setup.setup_deck).to be_kind_of CivilDeck
#       # expect()
#       debugger
#       puts 'end'
#     end
#   end

  # describe 'new' do
  #   xit 'is empty' do
  #     civil_deck = CivilDeck.new(card_prototype_ids: [])
  #     expect(civil_deck.card_prototype_ids).to eq []
  #   end
  # end

  # describe 'setup' do
  #   xit 'sets up its card_prototype_ids to given age' do
  #     civil_deck = CivilDeck.create
  #     civil_deck.setup('A')
  #     civil_deck.reload
  #     expect(civil_deck.card_prototype_ids).to eq Cardtype.civil_deck_ids('A')
  #   end
  # end

  # describe '' do
  #   xit 'is setup' do
  #     # civildeck = Civildeck.new
  #     # civildeck = stub('Civildeck', cards: [])
  #     # expect(civildeck.cards).to eq [1]

  #     toBeFound = stub('toBeFound', setup_civil_deck: true)

  #     game = Game.new
  #     card_prototype_ids = [1, 2]

  #     toBeFound.setup_civil_deck(game, card_prototype_ids)

  #     game.civil_decks.build
  #     expect(game.civil_decks.first).to be_kind_of CivilDeck
  #   end
  # end

# class Game

#   has_many :civil_decks

#   def setup_civil_deck
#     DeckSetup.new(self).setup_civil_deck
#     civil_decks.create(card_prototype_ids: Cardtype.civil_deck_ids(age))
#   end

#   has_many :cards

#   def setup_civil_deck
#     Cardtype.civil_deck.each do |cardtype|
#       cards.create(cardtype: cardtype)
#     end
#   end

# end

# class GameSetup
#   attr_accessor :game, :cardtypes

#   def initialize(args={})
#     @game      = args.fetch(:game)
#     @cardtypes = args.fetch(:cardtypes)
#   end

#   def setup_civil_deck
#     DeckSetup.new(game: game)
#     cardtypes.each do |cardtype|
#       game.cards.create(cardtype: cardtype)
#     end
#   end
# end

# class DeckSetup
#   # attr_accessor :game
#   attr_reader :game

#   def initialize(args)
#     @game = args[:game]
#   end

#   # def setup_deck
#   #   CivilDeck.new
#   # end

#   # def setup_civil_deck
#   #   CivilDeck.create(game: game, card_prototype_ids: card_prototype_ids)
#   # end

#   # def card_prototype_ids
#   #   Cardtype.civil_deck_ids(game.age)
#   # end
# end

# CivilDeckSetup.new(game: game, cardtype_ids: Cardtype.civil_deck_ids('A')).setup_civil_deck

# class MilitaryDeckSetup
#   attr_accessor :card_prototype_ids
#   def initialize(args={})
#     @card_prototype_ids = args.fetch[:cardtype_ids]
#   end
# end

# class SomeObject
#   attr_accessor :ids
#   def initialize(args={})
#     @ids = args.fetch(:ids).shuffle
#   end
#   def deal_ids(n)
#     dealed = []
#     n.times do
#       dealed << deal
#       break if empty?
#     end
#     dealed
#   end
#   def deal
#     @ids.delete(@ids.last)
#   end
#   def size
#     ids.size
#   end
#   def empty?
#     size == 0 ? true : false
#   end
# end
