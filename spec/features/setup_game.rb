require 'spec_helper'

feature 'Setup a new game' do

  given(:game) { FakeGame.new }

  # As a User
  # When I begin a new game
  background do
  end

  scenario 'it should be setup' do
    expect(game.is_setup?).to be_true
  end
end

class FakeGame
  def is_setup?
    true
  end
end
