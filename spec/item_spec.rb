require 'item'

describe Item do
  describe 'a new Item' do
    it 'has a level that is passed in' do
      level = 2
      item  = Item.new(level)

      expect(item.level).to eq 2
    end
  end
end
