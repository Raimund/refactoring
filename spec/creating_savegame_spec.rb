describe Savegame do

  before do
    File.delete('savegame') if File.exists?('savegame')
  end

  describe 'save' do
    it 'puts some information into a yaml file' do
      file = 'savegame'
      svg = Savegame.new
      expect(File.exists?(file)).to be_false

      svg.save

      expect(File.exists?(file)).to be_true
    end
  end

  describe 'save' do
    xit 'puts some information into a yaml file' do
      svg = Savegame.new

      svg.save

      loaded_savegame = File.read('savegame')
      expect(loaded_savegame).to eq 'test'
    end
  end

  describe '' do
    it '' do
      item1 = Item.new(1)
      item2 = Item.new(2)
      input = [item1, item2]

      Savegame.new(input: input).save

      loaded_savegame  = YAML.load(File.read('savegame'))
      fixture_savegame = YAML.load(File.read('spec/fixtures/savegame.yml'))

      expect(loaded_savegame).to have(2).items
    end
  end

end
