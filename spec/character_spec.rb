#require_relative File.join("../lib/*.rb")
require 'character'
require 'inventory'
require 'item'

describe Character do

  describe '#inventory' do
    it 'returns the inventory of its character' do
      rimnaud = Character.new

      expect(rimnaud.inventory).to be_kind_of Inventory
    end
  end

  describe '#craft' do
    it 'produces a specific item' do
      rimnaud = Character.new

      rimnaud.craft('Assault Shrieker')

      expect(rimnaud.inventory.items.size).to eq 1
    end
  end

end
