# require 'spec_helper'

# class Game < ActiveRecord::Base
#   has_one :cardrow

#   attr_accessor :cardrow,
#     :positions_to_remove,
#     :number_of_players,
#     :civil_deck

#   def initialize(args={})
#     @cardrow             = args.fetch(:cardrow, Array.new(13) { 'x' })
#     @positions_to_remove = args.fetch(:positions_to_remove, 0..2)
#     @number_of_players   = args.fetch(:number_of_players, 2)
#     @civil_deck          = args.fetch(:civil_deck, [])
#   end
# end

# class Cardrow < ActiveRecord::Base
#   belongs_to :game
# end

# # at the beginning of each players turn (except round 1)
# # find cardrow
# # find number of players
# # compute cards to remove
# # find first position to remove
# # remove card from position on cardrow
# # increment position to remove
# # repeat until done
# # save cardrow
# class Ufo
#   #attr_accessor :card_prototypes_array
#   attr_accessor :game,
#     :cardrow,
#     :number_of_players,
#     :positions_to_remove,
#     :last_cardrow_position,
#     :positions_to_move,
#     :focus,
#     :positions_to_refill,
#     :civil_deck

#   def initialize(args={})
#     # game
#     @game                  = args.fetch(:game)
#     @number_of_players     = game.number_of_players

#     # cardrow
#     @cardrow               = args.fetch(:cardrow, game.cardrow)
#     @positions_to_remove   = compute_positions_to_remove
#     @last_cardrow_position = compute_last_cardrow_position
#     @positions_to_move     = compute_positions_to_move
#     # leftmost_empty_cardrow_position
#     @focus                 = 0
#     @positions_to_refill   = compute_positions_to_refill

#     # civil deck
#     @civil_deck            = args.fetch(:civil_deck, game.civil_deck)
#   end

#   def setup_cardrow
# #    remove_cards
#     move_cards
#     refill_cardrow
#     cardrow
#   end

#   private

#   # support for removing cards

#   # def compute_positions_to_remove
#   #   case number_of_players
#   #   when 2 then 0..2
#   #   when 3 then 0..1
#   #   when 4 then 0..0
#   #   end
#   # end

#   # def remove_cards
#   #   positions_to_remove.each do |position|
#   #     remove(position)
#   #   end
#   # end

#   # def remove(position)
#   #   cardrow[position] = 'x'
#   # end

#   # support for moving cards

#   def compute_last_cardrow_position
#     cardrow.size - 1
#   end

#   # def compute_positions_to_move
#   #   case number_of_players
#   #   when 2 then 3..last_cardrow_position
#   #   when 3 then 2..last_cardrow_position
#   #   when 4 then 1..last_cardrow_position
#   #   end
#   # end

#   # def move_cards
#   #   positions_to_move.each do |position|
#   #     if card_on_position?(position)
#   #       move_card(position)
#   #     end
#   #   end
#   # end

#   # def card_on_position?(position)
#   #   cardrow[position] != 'x'
#   # end

#   # def move_card(position)
#   #   cardrow[focus] = cardrow[position]
#   #   move_focus_one_right
#   #   remove(position)
#   # end

#   # def move_focus_one_right
#   #   self.focus += 1
#   # end

#   # support for refilling cards

#   def refill_cardrow
#     positions_to_refill.each do |position|
#       refill_position(position)
#       move_focus_one_right
#     end
#   end

#   def compute_positions_to_refill
#     focus..last_cardrow_position
#   end

#   def refill_position(position)
#     cardrow[position] = draw
#   end

#   def draw
#     civil_deck.pop
#   end

#   # def find_cardrow
#   #   self.cardrow = Array.new(13) { 'x' }
#   # end

#   # def remove_cards(game)
#   #   game.cardrow_positions_to_clear.each do |position|
#   #     clear_cardrow_position(game, position)
#   #   end
#   # end

#   # private

#   # def clear_cardrow_position(game, position)
#   #   game.cardrow[position] = 'x'
#   # end

#   # returns the positions of the cardrow that are going to be discarded
#   # returns those positions as a range dependent on number of players of this game
#   # def cardrow_positions_to_clear
#   #   case number_of_players
#   #   when 2 then 0..2
#   #   when 3 then 0..1
#   #   when 4 then 0..0
#   #   end
#   # end
# end

# describe Ufo do

#   describe 'setup_cardrow' do
#     it 'put as many cards from civil deck as free spaces are on the cardrow' do
#       civil_deck = (Array.new(13) { |i| i + 1 }).reverse
#       game       = Game.new(civil_deck: civil_deck)

#       ufo        = Ufo.new(game: game)
#       cardrow    = Cardrow.new

#       expect(ufo.setup_cardrow)
#       .to eq [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]

#       expect(cardrow.setup_cardrow(game))

# #      expect(ufo.cardrow).to eq [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
#     end

#     xit 'put as many cards from civil deck as free spaces are on the cardrow' do
#       civil_deck = (Array.new(13) { |i| i + 1 }).reverse
#       game       = Game.new(civil_deck: civil_deck)

#       ufo        = Ufo.new(game: game)

#       expect(ufo.setup_cardrow)
#       .to eq [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]

# #      expect(ufo.cardrow).to eq [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
#     end
#   end

#   describe '#setup_cardrow' do
#     xit 'puts cards from civil deck onto free cardrow spots' do
#       cardrow = Array.new(13) { 'x' }

#       game    = Game.new(cardrow: cardrow, cardrow_positions_to_clear: 0..2)

#       ufo     = Ufo.new

#       expect { ufo.setup_cardrow(game) }
#         .to change(game, :cardrow)
#         .from(cardrow)
#         .to([1,2,3])
#     end

#     xit 'removes the first left cards dependant on number of players of game' do
#       game = Game.new(
#         cardrow: %w(1 2 3 x x x x x x x x x x),
#         number_of_players: 2,
#         cardrow_positions_to_clear: 0..2)

#       ufo  = Ufo.new

#       expect { ufo.remove_cards(game) }
#         .to change(game, :cardrow)
#         .from( %w(1 2 3 x x x x x x x x x x) )
#         .to  ( %w(x x x x x x x x x x x x x) )

#       # expect(ufo.cardrow).to be_nil
#       # ufo.find_cardrow
#       # expect(ufo.cardrow).to eq cardtype_ids_array

#       #ufo.remove_cards(card_prototypes_array)

#     end
#   end
# end
