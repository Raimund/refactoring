require 'inventory'
require 'character'
require 'item'

describe Inventory do
  describe '#items' do
    it 'returns a empty collection of its items as default' do
      rimnaud = Character.new
      inv     = Inventory.new(character: rimnaud)

      expect(inv.items).to eq []
    end

    it 'returns a collection of its items' do
      rimnaud = Character.new

      items = []
      blaster_pistol = Item.new(1)
      items << blaster_pistol
      blaster_rifle = Item.new(2)
      items << blaster_rifle

      inv = Inventory.new(character: rimnaud, items: items)

      expect(inv.items).to eq [blaster_pistol, blaster_rifle]
    end
  end

  describe '#player' do
    it 'returns the player it belongs to' do
      rimnaud = Character.new
      inv     = Inventory.new(character: rimnaud)

      expect(inv.character).to eq rimnaud
    end
  end
end
