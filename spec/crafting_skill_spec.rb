require 'crafting_skill'
require 'item'

describe CraftingSkill do
  describe '#craft' do
    it 'produces an item' do
      skill = CraftingSkill.new

      expect(skill.craft).to be_kind_of Item
    end
  end

  describe '#craft_specific_item' do
    it 'produces a certain item known by its name' do
      skill = CraftingSkill.new

      expect(skill.craft_specific_item).to be_kind_of Item
    end
  end

end
