class Character
  attr_accessor :inventory

  def initialize(args={})
    @inventory = Inventory.new(character: self)
  end

  def craft(args={})
    self.inventory.items << Item.new(1)
  end

  def show_inventory
    inventory.items.each do |item|
      puts "#{item.to_s}"
    end
  end
end
