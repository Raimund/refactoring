require_relative 'character'
require_relative 'inventory'
require_relative 'item'
require_relative 'savegame'

class Game
  attr_accessor :option, :rimnaud

  def initialize
    self.rimnaud = Character.new
    self.option  = '0815'
  end

  def save_game
    Savegame.new(input: rimnaud).save!
  end

  def load_game
    input = File.read('savegame')
    self.rimnaud = YAML.load input
    clear_screen
    display_main_menue
    display_user_hud
  end

  def start
    clear_screen
    display_main_menue
    display_user_hud
    repeat_request_user_action_until_exit
  end

  private

  def repeat_request_user_action_until_exit
    until option == 'e' do
      request_user_action
      resolve_user_action
      display_user_hud
    end
  end

  def clear_screen
    system 'clear'
  end

  def display_main_menue
    puts 'Options:'
    puts '(c)raft'
    puts '(i)nventory'
    puts '(e)xit'
    puts
    puts '(l)oad last game'
  end

  def display_user_hud
    puts 'Rimnaud - Level 1 - Vanguard - Armstech'
    puts "Inventory: [#{rimnaud.inventory.items.size}]"
  end

  def request_user_action
    self.option = STDIN.getc
  end

  def resolve_user_action
    case option
    when 'c' then rimnaud.craft
    when 'i' then rimnaud.show_inventory
    when 'e' then save_game
    when 'l' then load_game
    end
  end

end
