class Inventory
  attr_accessor :character, :items

  def initialize(args={})
    @character = args.fetch(:character)
    @items     = args.fetch(:items, [])
  end
end
