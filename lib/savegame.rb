require_relative 'item'

class Savegame

  attr_accessor :input

  def initialize(args={})
    self.input = args.fetch(:input, '')
  end

  def save!
    File.open('savegame', 'w') do |file|
      file.puts input.to_yaml
    end
  end
end
